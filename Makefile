DOCKER_BUILD_IMAGE=go-builder

# Creates the docker container for building go applications
init-docker-go-builder:
	docker build -t ${DOCKER_BUILD_IMAGE} ./docker/go-builder/

# build the go binary inside the docker image
build-kafka-producer:
	cd KafkaProducer && ./build.sh ${DOCKER_BUILD_IMAGE}
