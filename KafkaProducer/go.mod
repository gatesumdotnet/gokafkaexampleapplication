module KafkaProducer

go 1.13

require (
	github.com/confluentinc/confluent-kafka-go v1.1.0
	github.com/segmentio/kafka-go v0.3.4
)
