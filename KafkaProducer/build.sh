#!/usr/bin/env bash

DOCKER_BUILD_IMAGE=$1

DOCKER_BUILD_DIR=/go/src/kafkaProducer
OUTPUT_DIR=dist
BINARY_NAME=KafkaProducer

rm -Rf ${OUTPUT_DIR}/${BINARY_NAME}
# add "-e CGO_ENABLED=1 -e CC=arm-linux-gnueabihf-gcc -e GOARCH=arm" for arm cross compile
docker run -it --rm -v $(pwd):${DOCKER_BUILD_DIR} -w ${DOCKER_BUILD_DIR} \
  -e CGO_ENABLED=0 -e GOOS=linux \
  ${DOCKER_BUILD_IMAGE} \
  go build -o ${OUTPUT_DIR}/${BINARY_NAME}
