package main

import (
	"KafkaProducer/kafka"
	"context"
	"log"
)

func main() {
	_, err := kafka.Configure(
		[]string{"localhost:9092"},
		"test",
		"TestTopic",
	)
	if err != nil {
		log.Println(err)
	}

	err = kafka.Push(context.Background(), nil, []byte("test message"))
	if err != nil {
		log.Println(err)
	}

}
