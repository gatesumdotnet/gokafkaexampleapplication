#!/usr/bin/env bash

if [ ! -d examples ]; then
  # clone confluent example repository
  git clone https://github.com/confluentinc/examples
fi

# ensure correct branch is checked out
cd examples && \
git checkout 5.3.1-post && \
cd cp-all-in-one/ && \
docker-compose up -d --build

echo 'http://localhost:9021/clusters'
